package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	public CreateLead()  {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement eleClickcreatelead;
	@FindBy(id="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(id="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(id="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleClickCreate;

	public  CreateLead enterCompanyName (String data){		
		type(eleCompanyName,data);
		return this;
	}
		public  CreateLead enterFirstName (String data){		
			type(eleFirstName,data);
			return this;
		}
			public  CreateLead enterLastName (String data){		
				type(eleLastName,data);
				return this;
	}
			public  ViewLead clickCreate(){		
				click(eleClickCreate);
				return new ViewLead();
	}
}

