package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage2 extends ProjectMethods{

	public HomePage2() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT,using="CRM/SFA")
		WebElement eleClickcrmsfa;

	public MyHomePage clickCrmsfa() {		
		click(eleClickcrmsfa);
		return new MyHomePage();
	}








}
