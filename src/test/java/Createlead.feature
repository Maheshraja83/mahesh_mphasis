Feature: Create Lead
Background:
Given Open the Browser
And Max the Browser
And Set the TimeOut
And Launch the URL
And Enter the Username as DemoSalesManager
And Enter the Password as crmsfa
And Click on the Login Button

Scenario Outline: Positive Login

And Click Crmsfa
And Click Leads
And Click Create Lead
And Enter the Companyname as <companyName>
And Enter the Firstname as <firstName>
And Enter the Lastname as <lastName>
When Click on Create Button
Then Verify the Created Lead

Examples:
|companyName|firstName|lastName|
|Mphasis|Divya|Rajan|
|Mphasis|Mahesh|Rajamani|
|Mphasis|Hema|M|
