package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
public ChromeDriver driver;
	@Given("Open the Browser")
	public void open_the_Browser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("Max the Browser")
	public void max_the_Browser() {
		driver.manage().window().maximize();
	}

	@Given("Set the TimeOut")
	public void set_the_TimeOut() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Launch the URL")
	public void launch_the_URL() {
	    driver.get("http://www.leaftaps.com/opentaps");
	}

	@Given("Enter the Username as (.*)")
	public void enter_the_Username(String data) {
	   driver.findElementById("username").sendKeys(data);
	}

	@Given("Enter the Password as (.*)")
	public void enter_the_Password(String data) {
		driver.findElementById("password").sendKeys(data);
	}

	@Given("Click on the Login Button")
	public void click_on_the_Login_Button() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click Crmsfa")
	public void click_Crmsfa() {
	   driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click Leads")
	public void click_Leads() {
	 driver.findElementByLinkText( "Leads").click();
	}

	@Given("Click Create Lead")
	public void click_Create_Lead() {
		 driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter the Companyname as (.*)")
	public void enter_the_Companyname(String data){
	  driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}

	@Given("Enter the Firstname as (.*)")
	public void enter_the_Firstname(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
	}

	@Given("Enter the Lastname as (.*)")
	public void enter_the_Lastname(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}

	@When("Click on Create Button")
	public void click_on_Create_Button() {
	    driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify the Created Lead")
	public void verify_the_Created_Lead() {
	   System.out.println("Lead created successfully");
	}

}
